<?php

final class Calculate
{
    public function CalculateDays ($startDate, $trainingCount, $schedule)
    {
        $arr_days = array(
                    1 => 'Mon',
                    2 => 'Tue',
                    3 => 'Wed',
                    4 => 'Thu',
                    5 => 'Fri',
                    6 => 'Sat',
                    7 => 'Sun'
            );

        $dateParse = date_create_from_format('Y-m-d', $startDate);
        if(! $dateParse || $trainingCount < 1 || (array() == $schedule) || !$schedule) return false;

        $totalDays = 0;
        $goneTraningDays = 0;

        do
        {
            foreach($schedule as $num_day)
            {
                if($arr_days[$num_day] == date_format($dateParse,'D'))
                {
                    ++$goneTraningDays;
                }
            }
            $dateParse->add(new DateInterval('P1D'));
            ++$totalDays;
        }while($goneTraningDays < $trainingCount);
        return $totalDays;
    }
}